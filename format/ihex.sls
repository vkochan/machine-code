;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2024 Vadym Kochan <vadim4j@gmail.com>
;; SPDX-License-Identifier: MIT
#!r6rs

;; Routines for reading the Intel HEX file
;;
;; Intel HEX file consists of the lines with the following format:

;; :10246200464C5549442050524F46494C4500464C33
;; |||||||||||                              CC->Checksum
;; |||||||||DD->Data [9-N]
;; |||||||TT->Record Type [7-8]
;; |||AAAA->Address [3-6]
;; |LL->Record Length [1-2]
;; :->Colon [0]

(library (machine-code format ihex)
  (export
    ihex-image?
    open-ihex-image
    ihex-image-cs
    ihex-image-ip
    ihex-image-eip
    ihex-image-base
    ihex-image-cells

    ihex-cell-address
    ihex-cell-data
    ihex-cell?)
  (import
    (rnrs (6)))

  (define IHEX-DATA-RECORD               0)
  (define IHEX-EOF-RECORD                1)
  (define IHEX-EXT-SEGMENT-ADDR-RECORD   2)
  (define IHEX-START-SEGMENT-ADDR-RECORD 3)
  (define IHEX-EXT-LINEAR-ADDR-RECORD    4)
  (define IHEX-START-LINEAR-ADDR-RECORD  5)

  (define-record-type ihex-image
    (fields cs ip eip base cells))

  (define-record-type ihex-cell
    (fields address data))

  (define (hex->bytes str)
    (let loop ((bytes '()) (str str))
      (if (< (string-length str) 2)
        bytes
        (let ((byte (string->number (substring str 0 2) 16))
              (str (if (> (string-length str) 2)
                       (substring str 2 (string-length str))
                       "")))
          (loop (append bytes (list byte)) str)))))

  (define (substring->number str start count)
    (string->number (substring str start (fx+ start count)) 16))

  (define (substring->bytes str start count)
    (hex->bytes (substring str start (fx+ start count))))

  (define (sum-list lst)
    (let loop ((sum 0) (lst lst))
      (if (null? lst)
          sum
          (loop (+ sum (car lst)) (cdr lst)))))

  (define (open-ihex-image func)

    (define (verify-csum lnum len addr type data csum)
      (let ((sum (fx- 256
                      (fxand (+ len addr type (sum-list data))
                             #xFF))))
        (when (not (eqv? csum sum))
          (error 'verify-csum "Checksum mismatch" lnum csum sum))))

    (let ((port (func)))
      (let loop ((lnum 1) (line (get-line port)) (cs 0) (ip 0) (eip 0) (base 0) (recs '()) (eof? #f))
        (when (and (eof-object? line) (not eof?))
          (error 'open-ihex-image "EOF record is missing" lnum))
        (if (or eof? (eof-object? line))
            (make-ihex-image cs ip eip base (list-sort
                                              (lambda (a b)
                                                (< (ihex-cell-address a) (ihex-cell-address b)))
                                              recs))
            (begin
              (when (not (eq? #\: (string-ref line 0)))
                (error 'open-ihex-image "':' char is missing at the start of line" lnum line))
              (when (< (string-length line) 11)
                (error 'open-ihex-image "Line is too short (less than 11 chars)" lnum line))
              (let ((len (substring->number line 1 2)))
                (when (< (- (string-length line) 1)
                         (fx* (+ len 5) 2))
                  (error 'open-ihex-image "record data length overflow" lnum len))
                (let ((len len)
                      (addr (substring->number line 3 4))
                      (type (substring->number line 7 2))
                      (data (substring->bytes line 9 (fx* len 2)))
                      (csum (substring->number line (fx+ 9 (fx* len 2)) 2)))
                  (verify-csum lnum len addr type data csum)
                  (cond
                    ((eqv? type IHEX-DATA-RECORD)
                     (let ((rec (make-ihex-cell (fx+ base addr) (u8-list->bytevector data))))
                       (loop (+ lnum 1) (get-line port) cs ip eip base (append recs (list rec)) #f)))
                    ((eqv? type IHEX-EOF-RECORD)
                     (loop (+ lnum 1) (get-line port) cs ip eip base recs #t))
                    ((eqv? type IHEX-EXT-SEGMENT-ADDR-RECORD)
                     (when (not (eqv? (length data) 2))
                       (error 'open-ihex-image "ext segment address requires 2 data bytes" lnum data))
                     (let ((base (fxior (fxarithmetic-shift-left (list-ref data 0) 8)
                                        (fxarithmetic-shift-left (list-ref data 1) 4))))
                       (loop (+ lnum 1) (get-line port) cs ip eip base recs eof?)))
                    ((eqv? type IHEX-START-SEGMENT-ADDR-RECORD)
                     (when (not (eqv? (length data) 4))
                       (error 'open-ihex-image "start segment address requires 4 data bytes" lnum data))
                     (let ((cs (fx+ (fxarithmetic-shift-left (list-ref data 0) 8)
                                    (list-ref data 1)))
                           (ip (fx+ (fxarithmetic-shift-left (list-ref data 2) 8)
                                    (list-ref data 3))))
                       (loop (+ lnum 1) (get-line port) cs ip eip base recs eof?)))
                    ((eqv? type IHEX-EXT-LINEAR-ADDR-RECORD)
                     (when (not (eqv? (length data) 2))
                       (error 'open-ihex-image "ext linear address requires 2 data bytes" lnum data))
                     (let ((base (fxior (fxarithmetic-shift-left (list-ref data 0) 8)
                                        (fxarithmetic-shift-left (list-ref data 1) 16))))
                       (loop (+ lnum 1) (get-line port) cs ip eip base recs eof?)))
                    ((eqv? type IHEX-START-LINEAR-ADDR-RECORD)
                     (when (not (eqv? (length data) 4))
                       (error 'open-ihex-image "start linear address requires 4 data bytes" lnum data))
                     (let ((eip (+ (fxarithmetic-shift-left (list-ref data 0) 24)
                                   (fxarithmetic-shift-left (list-ref data 1) 16)
                                   (fxarithmetic-shift-left (list-ref data 2) 8)
                                   (list-ref data 3))))
                       (loop (+ lnum 1) (get-line port) cs ip eip base recs eof?)))
                    (else (error 'open-ihex-image "Invalid record type" lnum type))))))))))
)
