#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2024 Vadym Kochan <vadim4j@gmail.com>
;; SPDX-License-Identifier: MIT
#!r6rs

(import
  (rnrs (6))
  (machine-code format ihex)
  (machine-code tests check))

(define (open-ihex-from-string str)
  (open-ihex-image
    (lambda ()
      (open-string-input-port str))))

(let ((ihex (open-ihex-from-string ":00000001FF")))
  (check (ihex-image-cs ihex) => 0)
  (check (ihex-image-ip ihex) => 0)
  (check (ihex-image-eip ihex) => 0)
  (check (length (ihex-image-cells ihex)) => 0))

(let ((ihex (open-ihex-from-string
":080000000102030405060708D4
:080010000102030405060708C4
:00000001FF")))
  (check (ihex-image-cs ihex) => 0)
  (check (ihex-image-ip ihex) => 0)
  (check (ihex-image-eip ihex) => 0)
  (check (length (ihex-image-cells ihex)) => 2)
  (let ((r0 (list-ref (ihex-image-cells ihex) 0))
        (r1 (list-ref (ihex-image-cells ihex) 1)))
    (check (ihex-cell-address r0) => 0)
    (check (ihex-cell-data r0) => #vu8(1 2 3 4 5 6 7 8))

    (check (ihex-cell-address r1) => 16)
    (check (ihex-cell-data r1) => #vu8(1 2 3 4 5 6 7 8))))

(let ((ihex (open-ihex-from-string
":020000040000FA
:0100000001FE
:020000040001F9
:0100000002FD
:00000001FF")))
  (check (ihex-image-cs ihex) => 0)
  (check (ihex-image-ip ihex) => 0)
  (check (ihex-image-eip ihex) => 0)
  (check (length (ihex-image-cells ihex)) => 2)
  (let ((r0 (list-ref (ihex-image-cells ihex) 0))
        (r1 (list-ref (ihex-image-cells ihex) 1)))
    (check (ihex-cell-address r0) => 0)
    (check (ihex-cell-data r0) => #vu8(1))

    (check (ihex-cell-address r1) => #x10000)
    (check (ihex-cell-data r1) => #vu8(2))))

(let ((ihex (open-ihex-from-string
":0400000312345678E5
:0100000001FE
:00000001FF")))
  (check (ihex-image-cs ihex) => #x1234)
  (check (ihex-image-ip ihex) => #x5678)
  (check (ihex-image-eip ihex) => 0)
  (check (length (ihex-image-cells ihex)) => 1)
  (let ((r0 (list-ref (ihex-image-cells ihex) 0)))
    (check (ihex-cell-address r0) => 0)
    (check (ihex-cell-data r0) => #vu8(1))))

(let ((ihex (open-ihex-from-string
":0400000512345678E3
:0100000002FD
:00000001FF")))
  (check (ihex-image-cs ihex) => 0)
  (check (ihex-image-ip ihex) => 0)
  (check (ihex-image-eip ihex) => #x12345678)
  (check (length (ihex-image-cells ihex)) => 1)
  (let ((r0 (list-ref (ihex-image-cells ihex) 0)))
    (check (ihex-cell-address r0) => 0)
    (check (ihex-cell-data r0) => #vu8(2))))

(check-report)

(exit (if (check-passed? 32) 0 1))
